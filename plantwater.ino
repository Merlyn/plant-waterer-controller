/*

 It connects to an MQTT server then:
  - on 0 switches off relay
  - on 1 switches on relay
  - on 2 switches the state of the relay

  - sends 0 on off relay
  - sends 1 on on relay

 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.

 The current state is stored in EEPROM and restored on bootup

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "//no-comment";
const char* password = "karlisgay";
const char* mqtt_server = "192.168.1.15";

WiFiClient espClient;
PubSubClient client(espClient);

const char* inTopic = "home/query/+";
const char* inControlTopic = "home/control/water";
const int relay_pin = 0;
const int sensePin=A0;



void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
  delay(1000);
  }
  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  digitalWrite(LED_BUILTIN, LOW); //turn on LED
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if(startsWith(inControlTopic,topic)){
    if ((char)payload[0] == '0') {
      digitalWrite(relay_pin, LOW);   
      Serial.println("relay_pin -> LOW");
    } else if ((char)payload[0] == '1') {
      digitalWrite(relay_pin, HIGH);
      Serial.println("relay_pin -> HIGH");
    }
  }else{
     if (strcmp((char*)payload,"query water")) {
      int val = analogRead(sensePin);
      Serial.print("value: ");
      char buf[4];
      itoa(val,buf,10);
      Serial.println(buf);
  
      char outTopic[strlen(topic)+10];
      strcpy(outTopic,topic);
      strcat(outTopic,"/response");
      client.publish(outTopic, buf);
    }
  }
  digitalWrite(LED_BUILTIN, HIGH);  
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("home/notifaction", "Plant Water booted");
      // ... and resubscribe
      client.subscribe(inTopic);
      client.subscribe(inControlTopic);
      
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(relay_pin, OUTPUT);     // Initialize the relay pin as an output
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the relay pin as an output
  digitalWrite(relay_pin,LOW);
  digitalWrite(LED_BUILTIN, LOW);          // Blink to indicate setup
  delay(500);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(500);
  digitalWrite(LED_BUILTIN, LOW);  
 
  Serial.begin(115200);
  setup_wifi();                   // Connect to wifi
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}

bool startsWith(const char *pre, const char *str)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? false : memcmp(pre, str, lenpre) == 0;
}
